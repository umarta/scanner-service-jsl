<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'Api\UserController@doLogin')->name('login');
});
Route::prefix('master')->group(function () {
    Route::get('warehouse', 'Api\MasterController@getWarehouse');
});
Route::group(['middleware' => 'auth:api'], function () {

    Route::prefix('master')->group(function () {
        Route::get('locator', 'Api\MasterController@getLocator');
        Route::post('locator', 'Api\MasterController@locatorScan');

        Route::get('locator-slug', 'Api\MasterController@getLocatorSlug');
        Route::post('locator-slug', 'Api\MasterController@locatorScanSlug');


        // Route::get('warehouse', 'Api\MasterController@getWarehouse');
        Route::get('product-detail', 'Api\MasterController@getProductDetail');
        Route::get('product-list', 'Api\MasterController@getProduct');
    });

    Route::prefix('receive')->group(function () {
        Route::get('/', 'Api\ScanController@getReceiveList');
        Route::get('/doc/{id}', 'Api\ScanController@getPartList');
        Route::post('check', 'Api\ScanController@materialReceive');
        Route::post('scan', 'Api\ScanController@scanProduct');
    });
    Route::prefix('inout')->group(function () {
        Route::get('/', 'Api\InventoryMoveController@invMoveList');
        Route::get('/info/{slug}', 'Api\InventoryMoveController@getInOutInfo');
        Route::post('/create-header', 'Api\InventoryMoveController@createHeaderMove');
        Route::post('/check-stock', 'Api\InventoryMoveController@checkStock');
        Route::post('/act', 'Api\InventoryMoveController@materialOut');
        // Route::get('/doc/{id}', 'Api\ScanController@getPartList');
        // Route::post('check', 'Api\ScanController@materialReceive');
        // Route::post('scan', 'Api\ScanController@scanProduct');
    });


    Route::prefix('scan')->group(function () {
        Route::post('opname', 'Api\ScanController@doOpname');
        Route::post('check', 'Api\ScanController@check');
        Route::post('check-stock', 'Api\ScanController@checkStock');
        Route::prefix('move')->group(function () {
            Route::post('check', 'Api\InventoryMoveController@doCheck');
            Route::post('confirm', 'Api\InventoryMoveController@confirmTrx');
            Route::post('scan', 'Api\InventoryMoveController@scanAction');
            Route::post('edit-locator', 'Api\InventoryMoveController@editLocator');
            Route::get('active-list', 'Api\InventoryMoveController@getActiveTransaction');

            Route::prefix('assembly')->group(function () {
                Route::post('/', 'Api\InventoryMoveController@createHeader');
                Route::get('/penerimaan/active', 'Api\AssemblyController@getActive');
                Route::post('/penerimaan', 'Api\AssemblyController@scanAction');
                Route::get('/penerimaan/detail', 'Api\AssemblyController@getDetail');
                Route::post('/penerimaan/close', 'Api\AssemblyController@closePenerimaan');
            });
        });
    });
});
