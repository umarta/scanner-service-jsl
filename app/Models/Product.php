<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'adempiere.m_product';

    public function stock()
    {
        return $this->hasOne(StockOnHand::class, 'm_product_id', 'm_product_id');
    }
}
