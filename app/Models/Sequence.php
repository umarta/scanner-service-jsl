<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sequence extends Model
{
    protected $table = 'adempiere.z_sequence';
    protected $fillable = [
        'year_month',
        'type',
        'sequence'
    ];
}
