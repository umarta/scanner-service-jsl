<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    protected $table = 'adempiere.m_movement';

    public function detail()
    {
        return $this->hasMany(MovementDetail::class, 'm_movement_id', 'm_movement_id');
    }
}
