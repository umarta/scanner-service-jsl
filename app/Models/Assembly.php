<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assembly extends Model
{
    protected $table = 'adempiere.z_scan_assembly';

    public $fillable = [
        'scan_id',
        'locator_id',
        'status_penerimaan'
    ];
    
    public function locator()
    {
        return $this->hasOne(Locator::class,'m_locator_id','locator_id');
    }
}
