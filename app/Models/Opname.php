<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opname extends Model
{
    protected $table = 'adempiere.z_opname';

    protected $fillable = [
        'wh',
        'locator',
        'kodeproduk',
        'nama',
        'qty',
        'tglopname',
        'stok',
        'user_id'
    ];
    public $incrementing = false;

    public $timestamps = false;



}
