<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryMove extends Model
{
    protected $table = 'adempiere.z_invmove';

    public function in()
    {
        return $this->hasMany(InventoryMoveTo::class, 'invmove_id', 'id');
    }

    public function out()
    {
        return $this->hasMany(InventoryMoveFrom::class, 'invmove_id', 'id');
    }
}
