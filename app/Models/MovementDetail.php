<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovementDetail extends Model
{
    protected $table = 'adempiere.m_movementline';

    public function product()
    {
        return $this->belongsTo(Product::class, 'm_product_id', 'm_product_id');
    }

    public function locator()
    {
        return $this->belongsTo(Locator::class, 'm_locator_id', 'm_locator_id');
    }
    public function locatorTo()
    {
        return $this->belongsTo(Locator::class, 'm_locatorto_id', 'm_locator_id');
    }

}
