<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryMoveFrom extends Model
{
    protected $table = 'adempiere.z_invmovefrom';

    public function headerMove()
    {
        return $this->hasMany(InventoryMove::class, 'id', 'invmove_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_code','value');
    }
}
