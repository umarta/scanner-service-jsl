<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
    protected $table = 'z_param';

    public $timestamps = false;
    public $incrementing = false;

    public $primaryKey = 'kodeproduk';

}
