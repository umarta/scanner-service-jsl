<?php

namespace App\Models\Z;

use Illuminate\Database\Eloquent\Model;

class InOuZ extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'adempiere.z_inout';

    public $fillable = [
        'documentid', 'documentno', 'docstatus', 'idscan', 'received', 'name', 'keterangan', 'c_bpartner_id', 'warehouse', 'status'
    ];
    public $timestamps = false;
    public $incrementings = false;
    // protected $primaryKey = 'documentno';

    public function line()
    {
        return $this->hasMany(InOutLineZ::class, 'documentid', 'documentid');
    }
}
