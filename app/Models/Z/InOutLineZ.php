<?php

namespace App\Models\Z;

use Illuminate\Database\Eloquent\Model;

class InOutLineZ extends Model
{
    protected $table = 'adempiere.z_inoutline';
    public $fillable = [
        'documentid', 'product', 'noitem', 'qtydo', 'qtyscan', 'qtysls', 'qtyerror', 'name', 'dateitemscan', 'qtylbh','locator'
    ];
    public $timestamps = false;
    public $incrementing = false;

    public $primaryKey = 'documentid';
}
