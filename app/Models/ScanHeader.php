<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScanHeader extends Model
{
    protected $table = 'adempiere.z_scan_header';
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'transaksi_id',
        'warehouse_from_id',
        'warehouse_to_id',
        'kode_warehouse_from',
        'kode_warehouse_to',
        'doc_no',
        'doc_status',
        'author',
        'type_trx'
    ];

    public function detail(): object
    {
        return $this->hasMany(ScanDetail::class, 'scan_id', 'id');
    }

    public function assembly()
    {
        return $this->hasOne(Assembly::class, 'scan_id','id');
    }
}
