<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryMoveTo extends Model
{
    protected $table = 'adempiere.z_invmoveto';
}
