<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScanLog extends Model
{
    protected $table = 'adempiere.z_scan_log';

    public $incrementings = false;
    public $timestamps = false;
    public $primaryKey = 'qrcode_id';
}
