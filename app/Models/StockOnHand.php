<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockOnHand extends Model
{
    protected $table = 'adempiere.m_storageonhand';

    public function product()
    {
        return $this->hasOne(Product::class, 'm_product_id', 'm_product_id');
    }
    public function products()
    {
        return $this->hasMany(Product::class, 'm_product_id', 'm_product_id');
    }

    public function locator()
    {
        return $this->hasOne(Locator::class, 'm_locator_id', 'm_locator_id');
    }
}
