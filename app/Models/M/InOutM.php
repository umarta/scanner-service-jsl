<?php

namespace App\Models\M;

use App\Models\Partner;
use Illuminate\Database\Eloquent\Model;

class InOutM extends Model
{
    protected $connection = 'pgsql2';
    protected $table = 'adempiere.m_inout';
    public function line()
    {
        return $this->hasMany(InOutLineM::class, 'm_inout_id', 'm_inout_id');
    }

    public function partner()
    {
        return $this->hasOne(Partner::class, 'c_bpartner_id', 'c_bpartner_id');
    }
}
