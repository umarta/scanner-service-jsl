<?php

namespace App\Models\M;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class InOutLineM extends Model
{
    protected $connection = 'pgsql2';

    protected $table = 'adempiere.m_inoutline';

    public function product()
    {
        return $this->hasOne(Product::class, 'm_product_id', 'm_product_id');
    }
}
