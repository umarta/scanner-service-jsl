<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScanDetail extends Model
{

    protected $table = 'adempiere.z_scan_detail';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'scan_id',
        'no',
        'kode_produk',
        'locator_to_id',
        'kode_locator_to',
        'locator_from_id',
        'kode_locator_from',
        'qty_idem',
        'qty_scan',
        'product_name',
        'sku',
        
    ];

    public function header()
    {
        return $this->belongsTo(ScanHeader::class,'scan_id','id');
    }
}
