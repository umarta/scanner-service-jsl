<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locator extends Model
{
    protected $table = 'adempiere.m_locator';

    public function wh()
    {
        return $this->belongsTo(Warehouse::class, 'm_warehouse_id', 'm_warehouse_id');
    }
}
