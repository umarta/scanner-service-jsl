<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Locator;
use App\Models\Product;
use App\Models\StockOnHand;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterController extends Controller
{
    public function getWarehouse(Request $request)
    {
        $data = Warehouse::query();
        $src = $request->s;
        $low = strtolower($src);
        $exp = explode(' ', $low);
        if (strlen($request->s) > 0) {
            foreach ($exp as $value) {
                $data->whereRaw(
                    '(LOWER("name") ilike ? )',
                    ['%' . $value . '%']
                );
            }
        }

        $limit = strlen($request->limit) > 0 ? $request->limit : 10;

        $data = $data->where('isactive', 'Y')->paginate(100);
        $map = $data->map(function ($q) {
            return [
                'id' => $q->m_warehouse_id,
                'name' => $q->name
            ];
        });
        $page['current_page'] = $data->currentPage();
        $page['total_page'] = $data->lastPage();
        $page['total_data'] = $data->total();
        $page['next_page'] = $data->hasMorePages();

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $map,
            'header' => $page,
            'message' => ['loaded']

        ], 200);
    }

    public function getLocator(Request $request)
    {
        $data = Locator::query()->where('m_warehouse_id', $request->warehouse_id);

        $src = $request->s;
        $low = strtolower($src);
        $exp = explode(' ', $low);
        if (strlen($request->s) > 0) {
            foreach ($exp as $value) {
                $data->whereRaw(
                    '(LOWER("value") ilike ? )',
                    ['%' . $value . '%']
                );
            }
        }

        // $limit = strlen($request->limit) > 0 ? $request->limit : 10;

        $data = $data->paginate(1000);
        $map = $data->map(function ($q) {
            return [
                'id' => $q->m_locator_id,
                'name' => $q->value
            ];
        });
        $page['current_page'] = $data->currentPage();
        $page['total_page'] = $data->lastPage();
        $page['total_data'] = $data->total();
        $page['next_page'] = $data->hasMorePages();

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $map,
            'header' => $page,
            'message' => ['loaded']

        ], 200);
    }
    public function getLocatorSlug(Request $request)
    {
        $data = Locator::query()->whereHas('wh', function ($q) use ($request) {
            $q->where('value', $request->warehouse_id);
        });

        $src = $request->s;
        $low = strtolower($src);
        $exp = explode(' ', $low);
        if (strlen($request->s) > 0) {
            foreach ($exp as $value) {
                $data->whereRaw(
                    '(LOWER("value") ilike ? )',
                    ['%' . $value . '%']
                );
            }
        }

        // $limit = strlen($request->limit) > 0 ? $request->limit : 10;

        $data = $data->paginate(1000);
        $map = $data->map(function ($q) {
            return [
                'id' => $q->m_locator_id,
                'name' => $q->value
            ];
        });
        $page['current_page'] = $data->currentPage();
        $page['total_page'] = $data->lastPage();
        $page['total_data'] = $data->total();
        $page['next_page'] = $data->hasMorePages();

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $map,
            'header' => $page,
            'message' => ['loaded']

        ], 200);
    }

    public function locatorScan(Request $request)
    {
        $data = Locator::query()->where('m_warehouse_id', $request->warehouse_id)->where('value', 'ilike', $request->locator_id)->first();
        if (!$data) {
            return response()->json(
                [
                    'success' => false,
                    'code' => 200,
                    'message' => ['Locator not found'],
                    'data' => null
                ],
                200
            );
        }
        return response()->json(
            [
                'success' => true,
                'code' => 200,
                'message' => ['found'],
                'data' => [
                    'id' => $data->m_locator_id,
                    'name' => $data->value
                ]
            ],
            200
        );
    }
    public function locatorScanSlug(Request $request)
    {
        $data = Locator::query()->whereHas('wh', function ($q) use ($request) {
            $q->where('value', $request->warehouse_id);
        })->where('value', 'ilike', $request->locator_id)->first();
        if (!$data) {
            return response()->json(
                [
                    'success' => false,
                    'code' => 200,
                    'message' => ['Locator not found'],
                    'data' => null
                ],
                200
            );
        }
        return response()->json(
            [
                'success' => true,
                'code' => 200,
                'message' => ['found'],
                'data' => [
                    'id' => $data->m_locator_id,
                    'name' => $data->value
                ]
            ],
            200
        );
    }



    public function getProductDetail(Request $request)
    {
        $data = StockOnHand::query()->where('m_locator_id', $request->locator_id)->where('m_product_id', $request->product_id)->first();
        if (!$data) {
            return response()->json(
                [
                    'success' => false,
                    'code' => 200,
                    'message' => ['Product not found'],
                    'data' => []
                ],
                200
            );
        }

        $res = [
            'locator_id' => $data->m_locator_id,
            'product_id' => $data->m_product_id,
            'name' => $data->product->name,
            'name2' => $data->product->value,
            'qty' => $data->qtyonhand,
            'qty_opname' => 0
        ];
        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $res,
            'message' => ['loaded']
        ], 200);
    }

    public function getProduct(Request $request)
    {
        $data = Product::query();
        $src = $request->s;
        $low = strtolower($src);
        $exp = explode(' ', $low);

        if (strlen($request->s) > 0) {
            foreach ($exp as $value) {
                $data->whereRaw(
                    '(LOWER("name") ilike ? )',
                    ['%' . $value . '%']
                );
            }
        }
        $limit = strlen($request->limit) > 0 ? $request->limit : 10;

        $data = $data->paginate($limit);
        $map = $data->map(function ($q) {
            return [
                'id' => $q->m_product_id,
                'name' => $q->name,
                'code' => $q->value,
            ];
        });
        $page['current_page'] = $data->currentPage();
        $page['total_page'] = $data->lastPage();
        $page['total_data'] = $data->total();
        $page['next_page'] = $data->hasMorePages();

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $map,
            'header' => $page,
            'message' => ['loaded']
        ], 200);
    }
}
