<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\M\InOutLineM;
use App\Models\Locator;
use App\Models\M\InOutM;
use App\Models\Opname;
use App\Models\Param;
use App\Models\Product;
use App\Models\ScanLog;
use App\Models\StockOnHand;
use App\Models\Z\InOutLineZ;
use App\Models\Z\InOuZ;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ScanController extends Controller
{
    public function doOpname(Request $request)
    {
        $rules = [
            'warehouse_id' => 'required',
            'locator' => 'required',
            'product_code' => 'required',
            'qty_auto' => 'nullable|boolean',
            'qty_opname' => 'required_if:qty_auto,false',
            'is_scan' => 'required|boolean'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ], 200);
        $lc = Locator::query()->where('m_locator_id', $request->locator)->with('wh')->first();

        $exp = explode('#', $request->product_code);
        if (count($exp) == 3) {
            $code = collect($exp)->last();
        } else {
            $code = collect($exp)->first();
        }
        $data = Opname::query()->where('wh', $lc->wh->value)->where('locator', $lc->value)->where('kodeproduk', 'ilike', $code)->first();
        $product = Product::query()->where('value', 'ilike', $code)->first();
        $stock = DB::select("select sum(s.qtyonhand) as stok from adempiere.m_storageonhand s inner join adempiere.m_product p on s.m_product_id = p.m_product_id
                        where p.value ilike '$code'  group by p.value, p.name Having sum(s.qtyonhand) > 0 order by p.value");
        $save = [];
        $qty = $data->qty ?? 0;
        if ($request->is_scan) {
            if (!$data) {
                if (!$product) {
                    return response()->json([
                        'success' => false,
                        'code' => 404,
                        'message' => ['Product not found']
                    ], 200);
                }
                $input = [
                    'wh' => $lc->wh->value,
                    'locator' => $lc->value,
                    'kodeproduk' => $code,
                    'nama' => $product->name,
                    'qty' => $request->qty_auto == true ? 1 : $request->qty_opname,
                    'tglopname' => Carbon::now(),
                    'stok' => $stock ? round($stock[0]->stok, 4) : 0,
                    'user_id' => \Auth::user()->ad_user_id
                ];

                $save = Opname::query()->create($input);
                return response()->json([
                    'success' => true,
                    'code' => 201,
                    'data' => $save,
                    'message' => ['success']
                ], 200);
            }
            //        return $request->qty_auto == true ? 1 : $data->qty + 1;
            Opname::query()->where('wh', $lc->wh->value)->where('locator', $lc->value)->where('kodeproduk', 'ilike', $code)->update([
                'qty' => $request->qty_auto == true ? $data->qty + 1 : $data->qty + $request->qty_opname,
                'tglopname' => Carbon::now()->setTimezone('Asia/Jakarta')
            ]);

            $qty = ($request->qty_auto == false ? $data->qty + $request->qty_opname : $data->qty + 1);
        }


        $save = [
            'wh' => $data->wh ?? null,
            'locator' => $data->locator ?? null,
            'kodeproduk' => $product->value ?? null,
            'nama' => $product->name ?? null,
            'sku' => $product->sku ?? null,
            'qty' => (int)$qty ?? 0,
            'tglopname' => Carbon::now()->setTimezone('Asia/Jakarta'),
            'stok' => $stock ? round($stock[0]->stok, 4) : 0
        ];

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $save,
            'message' => ['success']
        ], 200);
    }

    public function check(Request $request)
    {

        $exp = explode('#', $request->product_code);
        if (count($exp) == 3) {
            $code = collect($exp)->last();
        } else {
            $code = collect($exp)->first();
        }

        $stock = DB::select("SELECT kodeproduk,locator,qty from adempiere.z_opname where kodeproduk ilike '%$code%'");

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $stock,
            'message' => ['success']
        ], 200);
    }

    public function checkStock(Request $request)
    {
        $exp = explode('#', $request->product_code);
        if (count($exp) == 3) {
            $code = collect($exp)->last();
        } else {
            $code = collect($exp)->first();
        }

        $stock = DB::select("select
                    p.value as kodeproduk,
                    p.name as namaproduk,
                    p.sku as sku,
                    l.value as locator,
                    w.value AS warehouse,
                    s.qtyonhand::float as qty
                    from adempiere.m_storageonhand s
                    INNER JOIN adempiere.m_product p on s.m_product_id = p.m_product_id
                    INNER JOIN adempiere.m_locator l ON s.m_locator_id = l.m_locator_id
                    INNER JOIN adempiere.m_warehouse w ON l.m_warehouse_id = w.m_warehouse_id
                    where p.value ilike '$code' and w.m_warehouse_id = '$request->warehouse_id' and s.qtyonhand > 0 order by p.value ");

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $stock,
            'message' => ['success']
        ], 200);
    }
    private function page($data): array
    {
        $page['current_page'] = $data->currentPage();
        $page['total_page'] = $data->lastPage();
        $page['total_data'] = $data->total();
        $page['next_page'] = $data->hasMorePages();
        return $page;
    }

    public function getReceiveList()
    {
        $data = InOuZ::query()->select('documentid', DB::raw('count(documentid) as doc_count'))->groupBy('documentid', 'status')->paginate(10);
        $map = $data->map(function ($q) {
            return [
                'documentid' => $q->documentid,
                'doc_count' => $q->doc_count,
            ];
        });

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $map,
            'header' => $this->page($data),
            'message' => ['loaded']

        ], 200);
    }

    public function getPartList($docid)
    {
        $data = InOuZ::query()->where('documentid', $docid)->with('line')->first();
        $DO = InOuZ::query()->where('documentid', $docid)->select('documentno')->get();

        if (!$data) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => ['data not found'],
                'data' => []
            ], 200);
        }

        $mapProduct = $data->line->map(function ($q, $key) {
            return [
                'noitem' => (int)$key + 1,
                'product' => $q->product,
                'name' => $q->name,
                'qtydo' => (int)$q->qtydo,
                'qtyscan' => (int)$q->qtyscan,
                'qtysls' => (int)$q->qtydo - (int)$q->qtyscan,

            ];
        });

        $res = [
            'documentid' => $data->documentid,
            'm_warehouse_id' => $data->m_warehouse_id,
            'c_bpartner_id' => $data->c_bpartner_id,
            'list_do' => $DO,
            'list_part' => $mapProduct
        ];
        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $res,
            'message' => ['success']
        ], 200);
    }

    public function materialReceive(Request $request)
    {
        $rules  = [
            'do_number' => 'required',
            'm_warehouse' => 'required',
            'm_locator' => 'nullable',

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);

        $scanDo = InOutM::query()->with('line.product', 'partner')->where('documentno', $request->do_number)->first();

        if (!$scanDo) {
            return response()->json([
                'success' => false,
                'code' => 404,
                'message' => ['Product not found']
            ], 200);
        }

        $getParam = Param::query()->where('kodeproduk', 'ilike', '%scanner%')->firstOrNew();
        $findData = InOuZ::query()->where('documentno', $request->do_number)->first();
        if (!$findData) {
            if (!$getParam) {
                $nomor = 1;
            } else {
                $nomor = (int)$getParam->nomor + 1;
                $nomor = sprintf('%010s', $nomor);
            }
        } else {
            $nomor = $findData->documentid;
        }

        if ($request->documentid) {
            $nomor =  $request->documentid;
        }

        $header = [
            'documentid' => $nomor,
            'documentno' => $request->do_number,
            'docstatus' => 'CO',
            'status' => 1,
            'idscan' => Auth::user()->name,
            'received' => Carbon::now(),
            'name' => $scanDo->partner->name,
            'keterangan' => '',
            'c_bpartner_id' => $scanDo->partner->value,
            'warehouse' => $request->m_warehouse

        ];
        if (!$findData) {
            $create = InOuZ::query()->create($header);

            $getParam->nomor = $nomor;
            $getParam->kodeproduk = 'SCANNER';
            $getParam->save();
        } else {
            $create = InOuZ::query()->where('documentno', $request->do_number)->update($header);
        }

        $ln = [];
        foreach ($scanDo->line as $k => $line) {
            $lines = [
                'noitem' => $k + 1,
                'documentid' => $nomor,
                'product' => $line->product->value,
                'name' => $line->product->name,
                'qtydo' => $line->movementqty,
                'dateitemscan' => null,
                'locator' => $request->m_locator ? $request->m_locator : 'INTRANSIT',
            ];
            $ln[] = $lines;
            $findLine = InOutLineZ::query()->where('documentid', $nomor)->where('product', $line->product->value)->first();
            if (!$findLine) {
                InOutLineZ::query()->create($lines);
            } else {
                $lines['qtydo'] = $findLine->qtydo  + $line->movementqty;
                InOutLineZ::query()->where('documentid', $nomor)->where('product', $line->product->value)->update($lines);
            }
        }
        $DO = InOuZ::query()->where('documentid', $nomor)->select('documentno')->get();

        $data = InOuZ::query()->where('documentid', $nomor)->with('line')->first();
        $mapProduct = $data->line->map(function ($q, $key) {
            return [
                'noitem' => (int)$key + 1,
                'product' => $q->product,
                'name' => $q->name,
                'qtydo' => (int)$q->qtydo,
                'qtyscan' => (int)$q->qtyscan,
                'qtysls' => (int)$q->qtydo - (int)$q->qtyscan,

            ];
        });
        $res = [
            'documentid' => $nomor,
            'm_warehouse_id' => $request->m_warehouse,
            'c_bpartner_id' => $scanDo->partner->value,
            'list_do' => $DO,
            'list_part' => $mapProduct
        ];


        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $res,
            'message' => ['success']
        ], 200);
    }

    public function scanProduct(Request $request)
    {
        $rules = [
            'product_code' =>  'required',
            'documentid' => 'required',
            'is_auto' => 'required',
            'qty' => 'nullable|required:is_autp,=,false'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);


        if ($request->is_auto) {
            $qty = 1;
        } else {
            $qty = $request->qty;
        }
        $exp = explode('#', $request->product_code);
        if (count($exp) == 3) {
            $qty = $exp[1];
            $code = collect($exp)->last();

            $log = ScanLog::query()->where('qrcode_id', $exp[0])->first();
            if ($log) {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'message' => [
                        'item sudah pernah discan'
                    ],
                    'data' => []
                ]);
            } else {
                $scanLog = new ScanLog();
                $scanLog->scan_id = $request->documentid;
                $scanLog->qrcode_id = $exp[0];
                $scanLog->datescan = Carbon::now();
                $scanLog->save();
            }
        } else {
            $code = collect($exp)->first();
        }

        $line = InOutLineZ::query()->where('product', $code)->where('documentid', $request->documentid)->first();

        if (!$line) {
            return response()->json([
                'success' => false,
                'code' => 404,
                'message' => ['Product not found']
            ], 200);
        }
        InOutLineZ::query()->where('product', $code)->where('documentid', $request->documentid)->update(
            [
                'qtyscan' => $line->qtyscan + $qty,
                'dateitemscan' => Carbon::now(),
                'locator' => $request->locator,
                'qtysls' => (int)$line->qtydo - (int)($line->qtyscan + $qty),

            ]
        );



        $line->qtyscan = (int) $line->qtyscan + $qty;
        $line->qtydo = (int) $line->qtydo;

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $line,
            'message' => ['success']
        ], 200);
    }
}
