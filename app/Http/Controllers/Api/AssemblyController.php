<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Assembly;
use App\Models\ScanDetail;
use App\Models\ScanHeader;
use App\Models\Locator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AssemblyController extends Controller
{
    public function getActive(Request $request)
    {   
        $data = ScanHeader::query()->where('warehouse_to_id',$request->warehouse_id)
        ->where('type_trx','Assembly')->whereHas('assembly',function($q){
            $q->where('status_penerimaan',false);
            })->get();
        $res = $data->map(function($q){
            return  [
                'id' => $q->id,
                'name'=> $q->doc_no,
            ];
        });
        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $res
        ]);
    }
    
    public function getDetail(Request $request)
    {
        $data = ScanDetail::query()->whereHas('header',function($q)use($request){
            $q->where('id',$request->transaction_id);
        })->get();

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $data
        ]);
    }

    public function scanAction(Request $request)
    {
        $rules = [
            'kode_produk' => 'required',
            'transaksi_id' => 'required',
            'qty' => 'required',
            'type' => 'required',
            'locator_id' => 'required',
            'is_edit' => 'nullable|boolean'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => null
            ]);
            $scanDetail = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
                $q->where('id', $request->transaksi_id);
                $q->where('type_trx',$request->type);
            });
            $scanDetail = $scanDetail->where('kode_produk', 'ilike', $request->kode_produk);
            $scanDetail = $scanDetail->first();
            
            if (!$scanDetail) {
                return response()->json([
                    'success' => false,
                    'code' => 404,
                    'message' => [
                        'produk tidak ditemukan'
                    ],
                    'data' => []
                ]);
            }
            
            $getLocator = Locator::query()->where('m_locator_id',$request->locator_id)->where('m_warehouse_id',$scanDetail->header->warehouse_to_id)->first();
            
            if (!$getLocator) {
                return response()->json([
                    'success' => false,
                    'code' => 404,
                    'message' => [
                        'Locator tidak ditemukan'
                    ],
                    'data' => []
                ]);
            }
            
            if ($request->is_edit) {
                $scanDetail->update([
                    'qty_scan' => $request->qty
                ]);
            } else {
                $scanDetail->update([
                    'qty_scan' => $scanDetail->qty_scan + $request->qty
                ]);
            }
            if(!$scanDetail->locator_to_id){
                $scanDetail->update([
                    'locator_to_id'=>$request->locator_id,
                    'kode_locator_to'=>$getLocator->value
                    ]);
            }
            
            $getActualQty = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
                $q->where('id', $request->transaksi_id);
                $q->where('type_trx',$request->type);
            })->get();

            return response()->json([
                'success' => true,
                'code' => 200,
                'data' => $getActualQty,
                'message' => ['success']
            ]);
    }
    
    public function closePenerimaan(Request $request)
    {
        $rules = [
            'transaction_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => null
            ]);

        $data = Assembly::query()->where('scan_id',$request->transaction_id)->first();
        
        if (!$data) {
            return response()->json([
                'success' => false,
                'code' => 404,
                'message' => [
                    'Transaksi tidak ditemukan'
                ],
                'data' => []
            ]);
        }
        
        $data->update([
            'status_penerimaan'=>true
            ]);
            
        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => ['Success menutup transaksi'],
            'data' => null
        ]);

    }
}
