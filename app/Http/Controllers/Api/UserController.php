<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function doLogin(Request $request): JsonResponse
    {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 200);
        $user = User::query()->where('name', $request->username)->where('password', $request->password)->first();
        if (!$user) {
            return response()->json(
                [
                    'success' => false,
                    'code' => 404,
                    'message' => ['User not found'],
                    'data' => null
                ], 200);
        }
        $res = [
            'user' => [
                'id' => $user->ad_user_id,
                'name' => $user->name,
            ],

            'token' => $user->createToken('scanner')->accessToken
        ];
        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $res
        ], 200);


    }
}
