<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\InventoryMove;
use App\Models\InventoryMoveFrom;
use App\Models\InventoryMoveTo;
use App\Models\Locator;
use App\Models\Product;
use App\Models\Movement;
use App\Models\ScanDetail;
use App\Models\ScanHeader;
use App\Models\Sequence;
use App\Models\StockOnHand;
use App\Models\Warehouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Param;

class InventoryMoveController extends Controller
{
    public function getActiveTransaction(Request $request)
    {

        if ($request->has('transaction_id')) {
            $checkLocal = ScanHeader::query()->where('doc_status', 'active');
            if ($request->has('type')) {
                $checkLocal = $checkLocal->where('type_trx', $request->type);
                if ($request->type != 'Assembly') {
                    $checkLocal->where('transaksi_id', $request->transaction_id);
                } else {
                    $checkLocal->where('id', $request->transaction_id);
                }
            }
            $checkLocal = $checkLocal->first();
            $result['header'] = collect($checkLocal)->forget('detail');
            $result['detail'] = ScanDetail::query()->whereHas('header', function ($q) use ($checkLocal, $request) {
                if ($request->type != 'Assembly') {
                    $q->where('transaksi_id', $checkLocal->transaksi_id);
                } else {
                    $q->where('id', $request->transaction_id);
                }
                if ($request->has('type')) {
                    $q->where('type_trx', $request->type);
                }
                $q->where('doc_status', 'active');
            })->get();
            return response()->json([
                'success' => true,
                'code' => 200,
                'data' => $result,
                'message' => ['success']
            ]);
        }

        $data = ScanHeader::query()->where('doc_status', 'active')->where('type_trx', $request->type)->distinct('transaksi_id')->where('warehouse_from_id', $request->warehouse_id)->get();
        $map = $data->map(function ($q) use ($request) {
            return [
                'id' => $request->type != 'Assembly' ? $q->transaksi_id : $q->id,
                'name' => $request->type != 'Assembly' ? Carbon::parse($q->created_at)->format('d-m-Y') . ' # ' . $q->transaksi_id : $q->doc_no
            ];
        });
        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $map
        ]);
    }

    public function doCheck(Request $request): object
    {
        $rules = [
            'doc_no' => 'required',
            'type' => 'required|in:update,new',
            'transaksi_id' => 'required_if:type,=,update',
            'move_type' => 'required',
            'warehouse_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => null
            ]);
        $data = Movement::query()
            ->where('documentno', $request->doc_no)
            ->with([
                'detail' => function ($q) {
                    $q->with('product');
                    $q->with('locator');
                    $q->with('locatorTo');
                }
            ])
            ->where('docaction', 'CO');
        $moveType = $request->move_type == 'Penerimaan Barang' ? 'm_warehouseto_id' : 'm_warehouse_id';
        $data = $data->where($moveType, $request->warehouse_id);

        $data = $data->first();
        if (!$data) {
            return response()->json([
                'code' => 400,
                'message' => ['Document not found'],
                'data' => null
            ]);
        }
        $result = [];
        $checkLocal = ScanHeader::query()->where('doc_no', $request->doc_no)->first();

        if (!$checkLocal) {
            if ($data->detail->count() > 0) {
                $transaksi_id = $request->type == 'new' ? (ScanHeader::query()->where('type_trx', 'Pengeluaran Barang')->latest()->first() ? ScanHeader::query()->where('type_trx', 'Pengeluaran Barang')->latest()->first()->transaksi_id + 1 : 1) : $request->transaksi_id;
                $res = [
                    'doc_no' => $data->documentno,
                    'warehouse_from_id' => $data->detail->first()->locator->wh->m_warehouse_id,
                    'warehouse_to_id' => $data->detail->first()->locatorTo->wh->m_warehouse_id,
                    'kode_warehouse_from' => $data->detail->first()->locator->wh->value,
                    'kode_warehouse_to' => $data->detail->first()->locatorTo->wh->value,
                    'doc_status' => 'active',
                    'transaksi_id' => $transaksi_id,
                    'author' => Auth::id(),
                    'type_trx' => $request->move_type
                ];
                $movement = ScanHeader::query()->create($res);

                $getDetail = ScanDetail::query()->whereHas('header', function ($q) use ($transaksi_id) {
                    $q->where('transaksi_id', $transaksi_id);
                })->get();
                $ignoner = [];
                if ($getDetail->count() > 0) {
                    foreach ($getDetail as $detailLocal) {
                        foreach ($data->detail as $detailMovement) {
                            if ($detailLocal->kode_produk == $detailMovement->product->value) {
                                array_push($ignoner, $detailLocal->kode_produk);
                                $detailLocal->update([
                                    'qty_idem' => $detailLocal->qty_idem + $detailMovement->movementqty
                                ]);
                            }
                        }
                    }
                }
                $detail = $data->detail->map(function ($q, $key) use ($movement) {
                    return [
                        'scan_id' => $movement->id,
                        'no' => $key + 1,
                        'kode_produk' => $q->product->value,
                        'product_name' => $q->product->name,
                        'sku' => $q->product->sku,
                        'locator_to_id' => $q->m_locatorto_id,
                        'kode_locator_to' => $q->locatorTo->value,
                        'locator_from_id' => $q->m_locator_id,
                        'kode_locator_from' => $q->locator->value,
                        'qty_idem' => $q->movementqty,
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                });
                $toBeInsert = $detail->whereNotIn('kode_produk', $ignoner)->toArray();
                if (count($toBeInsert) > 0) ScanDetail::query()->insert($toBeInsert);
                $result['header'] = $movement;
                $result['detail'] = ScanDetail::query()->whereHas('header', function ($q) use ($transaksi_id) {
                    $q->where('transaksi_id', $transaksi_id);
                })->get();
            }
        } else {
            $result['header'] = collect($checkLocal)->forget('detail');
            $result['detail'] = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
                $q->where('transaksi_id', $request->transaksi_id);
            })->get();
        }
        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $result,
            'message' => ['success']
        ]);
    }

    public function editLocator(Request $request): object
    {
        $rules = [
            'kode_locator' => 'required',
            'scan_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);
        $scanDetail = ScanDetail::query()->where('scan_id', $request->scan_id)->first();
        if (!$scanDetail) {
            return response()->json([
                'code' => 404,
                'message' => ['Document not found'],
                'data' => []
            ]);
        }
        $getLocator = Locator::query()->where('value', $request->kode_locator)->first();
        if (!$getLocator) {
            return response()->json([
                'code' => 404,
                'message' => ['Locator not found'],
                'data' => []
            ]);
        }
        ScanDetail::query()->where('scan_id', $request->scan_id)->update([
            'locator_to_id' => $getLocator->m_locator_id,
            'kode_locator_to' => $getLocator->value
        ]);

        ScanHeader::query()->where('id', $request->scan_id)->update([
            'warehouse_to_id' => $getLocator->wh->m_warehouse_id,
            'kode_warehouse_to' => $getLocator->wh->value,
        ]);

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => [
                'locator_id' => $getLocator->m_locator_id,
                'kode_locator' => $getLocator->value,
                'warehouse_to_id' => $getLocator->wh->m_warehouse_id,
                'kode_warehouse_to' => $getLocator->wh->value,
            ],
            'message' => ['success']
        ]);
    }

    public function scanAction(Request $request)
    {
        $rules = [
            'kode_produk' => 'required',
            'transaksi_id' => 'required',
            'qty' => 'required',
            'type' => 'required',
            'is_edit' => 'nullable|boolean',
            'locator_id' => 'required_if:type,=,Assembly',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);

        $scanDetail = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
            if ($request->type != 'Assembly') {

                $q->where('transaksi_id', $request->transaksi_id);
            } else {
                $q->where('id', $request->transaksi_id);
            }
            $q->where('type_trx', $request->type);
        });

        $scanDetail = $scanDetail->where('kode_produk', 'ilike', $request->kode_produk);
        $scanDetail = $scanDetail->first();


        if ($request->has('type')) {
            switch ($request->type) {
                case "Pengeluaran Barang":


                    if (!$scanDetail) {
                        return response()->json([
                            'success' => false,
                            'code' => 404,
                            'message' => [
                                'produk tidak ditemukan'
                            ],
                            'data' => []
                        ]);
                    }
                    if ($request->is_edit) {
                        $scanDetail->update([
                            'qty_scan' => $request->qty
                        ]);
                    } else {
                        $scanDetail->update([
                            'qty_scan' => $scanDetail->qty_scan + $request->qty
                        ]);
                    }
                    $getActualQty = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
                        $q->where('transaksi_id', $request->transaksi_id);
                        $q->where('type_trx', 'Pengeluaran Barang');
                    })->get();

                    return response()->json([
                        'success' => true,
                        'code' => 200,
                        'data' => $getActualQty,
                        'message' => ['success']
                    ]);
                    break;
                case "Penerimaan Barang":


                    if (!$scanDetail) {
                        return response()->json([
                            'success' => false,
                            'code' => 404,
                            'message' => [
                                'produk tidak ditemukan'
                            ],
                            'data' => []
                        ]);
                    }
                    if ($request->is_edit) {
                        $scanDetail->update([
                            'qty_scan' => $request->qty
                        ]);
                    } else {
                        $scanDetail->update([
                            'qty_scan' => $scanDetail->qty_scan + $request->qty
                        ]);
                    }
                    $getActualQty = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
                        $q->where('transaksi_id', $request->transaksi_id);
                        $q->where('type_trx', 'Penerimaan Barang');
                    })->get();

                    return response()->json([
                        'success' => true,
                        'code' => 200,
                        'data' => $getActualQty,
                        'message' => ['success']
                    ]);
                    break;

                case "Assembly":
                    $getHeader = ScanHeader::query()->where('id', $request->transaksi_id)->where('type_trx', 'Assembly')->where('doc_status', 'active')->first();
                    if (!$getHeader) {
                        return response()->json([
                            'success' => false,
                            'code' => 400,
                            'message' => ['Transaksi tidak ditemukan'],
                            'data' => []
                        ]);
                    }

                    $getProduct = Product::query()->where('value', $request->kode_produk)->first();
                    $getLocator = Locator::query()->where('m_locator_id', $request->locator_id)->first();

                    if (!$getLocator) {
                        return response()->json([
                            'success' => false,
                            'code' => 400,
                            'message' => ['Invalid Locator'],
                            'data' => null
                        ]);
                    }
                    if (!$request->is_edit) {
                        $input = [
                            'scan_id' => $getHeader->id,
                            'no' => $getHeader->detail->count() + 1,
                            'kode_produk' => $getProduct->value,
                            'product_name' => $getProduct->name,
                            'sku' => $getProduct->sku,
                            'qty_scan' => 0,
                            'locator_to_id' => null,
                            'kode_locator_to' => null,
                            'locator_from_id' => $request->locator_id,
                            'kode_locator_from' => $getLocator->value,
                            'qty_idem' => $request->qty,
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        if (!$scanDetail) {
                            ScanDetail::query()->create($input);
                        } else {
                            $scanDetail->update([
                                'qty_idem' => $scanDetail->qty_idem + $request->qty
                            ]);
                        }

                        $getActualQty = ScanDetail::query()->whereHas('header', function ($q) use ($request) {
                            $q->where('id', $request->transaksi_id);
                            $q->where('type_trx', 'Assembly');
                        })->orderBy('id', 'desc')->get();

                        return response()->json([
                            'success' => true,
                            'code' => 200,
                            'data' => $getActualQty,
                            'message' => ['success']
                        ]);
                    }
                    break;
            }
        }
    }

    public function fetchInOutData(Request $request): object
    {
        $rules = [
            'doc_no' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);
    }

    public function confirmTrx(Request $request): object
    {
        $rules = [
            'transaction_id' => 'required',
            'type' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);

        $data = ScanHeader::query()->where('type_trx', $request->type);
        if ($request->type != 'Assembly') {

            $data = $data->where('transaksi_id', $request->transaction_id);
        } else {
            $data = $data->where('id', $request->transaction_id);
        }

        $data = $data->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'code' => 404,
                'message' => [
                    'transaksi tidak ditemukan'
                ],
                'data' => []
            ]);
        }

        $data->update([
            'doc_status' => 'complete'
        ]);

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => [
                'Berhasil selesaikan pekerjaan'
            ],
            'data' => []
        ]);
    }
    private function page($data): array
    {
        $page['current_page'] = $data->currentPage();
        $page['total_page'] = $data->lastPage();
        $page['total_data'] = $data->total();
        $page['next_page'] = $data->hasMorePages();
        return $page;
    }


    public function createHeader(Request $request): object
    {
        $rules = [
            'warehouse_to_id' => 'required',
            'warehouse_from_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => []
            ]);

        $getWH = Warehouse::query()->whereIn('m_warehouse_id', [$request->warehouse_to_id, $request->warehouse_from_id])->get();
        if ($getWH->count() != 2) {
            return response()->json([
                'success' => false,
                'code' => 404,
                'message' => [
                    'Gudang tujuan tidak ditemukan'
                ],
                'data' => []
            ]);
        }
        $seq = Sequence::query()->where('type', 'Assembly')->where('year_month', date('ym'))->latest()->first();
        $transaksi_id = $seq ? $seq->sequence + 1 : Sequence::query()->create(['year_month' => date('ym'), 'type' => 'Assembly', 'sequence' => 1])->sequence;

        $input = [
            'transaksi_id' => $transaksi_id,
            'warehouse_from_id' => $request->warehouse_from_id,
            'warehouse_to_id' => $request->warehouse_to_id,
            'kode_warehouse_from' => $getWH->where('m_warehouse_id', $request->warehouse_from_id)->first()->value,
            'kode_warehouse_to' => $getWH->where('m_warehouse_id', $request->warehouse_to_id)->first()->value,
            'doc_no' => 'ASS.' . date('ym') . '.' . sprintf('%04s', $transaksi_id),
            'type_trx' => 'Assembly',
            'doc_status' => 'active',
            'author' => Auth::id(),
        ];
        if ($seq) {
            $seq->update([
                'sequence' => $seq->sequence + 1
            ]);
        }
        $save = ScanHeader::query()->create($input);

        return response()->json([
            'success' => true,

            'data' => $save,
            'code' => 200,
            'message' => ['success']
        ]);
    }

    public function movementList()
    {
    }

    public function checkStock($product_code, $locator)
    {
        $exp = explode('#', $product_code);
        if (count($exp) == 3) {
            $code = collect($exp)->last();
        } else {
            $code = collect($exp)->first();
        }
        return  StockOnHand::query()->select(DB::raw('sum(qtyonhand)::integer as qty'))->where('m_locator_id', $locator)->whereHas('products', function ($q) use ($code) {
            $q->where('value', 'ilike', $code);
        })->groupBy('m_product_id')->havingRaw('sum(qtyonhand)::integer > 0')
            ->first();
    }

    public function checkWH($type, $wh)
    {
        if ($type == 'id') {
            $param = 'm_warehouse_id';
        } else {
            $param = 'value';
        }
        return Warehouse::query()->where($param, $wh)->first();
    }

    public function invMoveList()
    {

        $data = InventoryMove::query()->where('status', 1)->paginate(10);
        $map = $data->map(function ($q) {
            return [
                'id' => $q->id,
                'documentno' => $q->documentno,
                'document_date' => $q->document_date,
                'from_wh' => $q->from_wh,
                'to_wh' => $q->to_wh,
                'status' => $q->status == 1 ? 'active' : 'complete'
            ];
        });
        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $map,
            'header' => $this->page($data),
            'message' => ['loaded']

        ], 200);
    }

    public function createHeaderMove(Request $request)
    {
        $rules  = [
            'wh_from' => 'required',
            'wh_to' => 'required',
        ];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => null
            ]);
        $getParam = Param::query()->where('kodeproduk', 'ilike', '%invmove%')->firstOrNew();

        // if (!$findData) {
        if (!$getParam) {
            $nomor = 1;
        } else {
            $nomor = (int)$getParam->nomor + 1;
        }

        $documentno = sprintf('%010s', $nomor);

        // } else {
        //     $nomor = $findData->documentid;
        // }

        if ($request->documentid) {
            $nomor =  $request->documentid;
        }

        $checkWHOut = $this->checkWH('id', $request->wh_from);
        $checkWHIn = $this->checkWH('id', $request->wh_to);

        if (!$checkWHIn || !$checkWHOut) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => [
                    'invalid warehouse'
                ],
                'data' => null
            ]);
        }
        $record = new InventoryMove();
        $record->document_date = Carbon::now();
        $record->from_wh = $checkWHOut->value;
        $record->to_wh = $checkWHIn->value;
        $record->status = 1;
        $record->save();

        $getParam->nomor = $record->id;
        $getParam->kodeproduk = 'INVMOVE';
        $getParam->save();

        $record->documentno = sprintf('%010s', $record->id);
        $record->save();

        return response()->json([
            'success' => true,
            'data' => $record,
            'code' => 200,
            'message' => ['success']
        ]);
    }
    public function getLocator($type, $locator)
    {
        if ($type == 'id') {
            $param = 'm_locator_id';
        } else {
            $param = 'value';
        }

        return Locator::query()->where($param, $locator)->first();
    }
    public function materialOut(Request $request)
    {
        $rules = [
            'move_id' => 'required',
            'type' => 'required|in:in,out',
            'locator' => 'required',
            'qty' => 'required|numeric',
            'product_code' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 400,
                'message' => $validator->errors()->all(),
                'data' => null
            ]);
        $getMoveId = InventoryMove::query()->where('documentno', $request->move_id)->first();
        if (!$getMoveId) {
            return response()->json([
                'success' => false,
                'code' => 404,
                'message' => [
                    'move id not found'
                ],
                'data' => null
            ]);
        }
        $exp = explode('#', $request->product_code);
        $qty = $request->qty;
        if (count($exp) == 3) {
            $qty = $exp[1];
            $code = collect($exp)->last();
        } else {
            $code = collect($exp)->first();
        }

        if ($request->type == 'out') {
            $wh = $this->checkWH('value', $getMoveId->from_wh);
            $locator = $this->getLocator('id', $request->locator);

            if ($wh->m_warehouse_id != $locator->m_warehouse_id) {
                return response()->json([
                    'success' => false,
                    'code' => 404,
                    'message' => [
                        'Locator tidak valid'
                    ],
                    'data' => null
                ]);
            }
            $checkStock = $this->checkStock($code, $request->locator);

            if (!empty($checkStock)) {
                $record = InventoryMoveFrom::query()->where('invmove_id', $getMoveId->id)->where('product_code', $code)->where('locator', $locator->value)->firstOrNew();
                $record->invmove_id = $request->move_id;
                $record->locator = $locator->value;
                $record->product_code = $code;
                if (!empty($record->id)) {
                    $qtyInsert = $record->qty + $qty;
                    if ($checkStock->qty - ($qtyInsert) < 0) {
                        return response()->json([
                            'success' => false,
                            'code' => 400,
                            'message' => [
                                'stok tidak mencukupi'
                            ],
                            'data' => null
                        ]);
                    }
                    $record->qty = $record->qty + $qty;
                } else {
                    if ($checkStock->qty - ($qty) < 0) {
                        return response()->json([
                            'success' => false,
                            'code' => 400,
                            'message' => [
                                'stok tidak mencukupi'
                            ],
                            'data' => null
                        ]);
                    }
                    $record->qty = $qty;
                }
                $record->save();
                $record->product_name = $record->product->name;
                $record->sku = $record->product->sku;
                $mapRes = $this->moveInfo($request->move_id);
                return response()->json([
                    'success' => true,
                    'code' => 200,
                    'data' => [
                        'part_list' => $mapRes,
                        'scan_result' => $this->moveInfo($request->move_id, $code)
                    ],
                    'message' => ['success']
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'message' => [
                        'produk tidak ditemukan di lokator ini'
                    ],
                    'data' => null
                ]);
            }
        } else {
            $record = InventoryMoveFrom::query()->where('invmove_id', $getMoveId->id)->where('product_code', $code)->get();
            $recordTo = InventoryMoveTo::query()->where('invmove_id', $getMoveId->id)->where('product_code', $code)->get();

            if ($record->count() < 1) {
                return response()->json([
                    'success' => false,
                    'code' => 404,
                    'message' => [
                        'Produk tidak ditemukan'
                    ],
                    'data' => null
                ]);
            }

            //get total
            $sum = $record->pluck('qty')->sum();
            $sumTo = $recordTo->pluck('qty')->sum();

            // $sum = $sum + $sumTo;

            if (($sum - ($qty + $sumTo)) < 0) {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'message' => [
                        'Stok tidak mencukupi'
                    ],
                    'data' => null
                ]);
            }
            $locator = $this->getLocator('id', $request->locator);


            $recordMove = InventoryMoveTo::query()->where('invmove_id', $getMoveId->id)->where('product_code', $code)->where('locator', $locator->value)->firstOrNew();
            $recordMove->invmove_id = $request->move_id;
            $recordMove->locator = $locator->value;
            $recordMove->product_code = $code;
            if (!empty($recordMove->id)) {
                $qtyInsert = $recordMove->qty + $qty;
                if ($sum - ($qtyInsert) < 0) {
                    return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => [
                            'stok tidak mencukupi'
                        ],
                        'data' => null
                    ]);
                }
                $recordMove->qty = $recordMove->qty + $qty;
            } else {
                if ($sum - ($qty) < 0) {
                    return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => [
                            'stok tidak mencukupi'
                        ],
                        'data' => null
                    ]);
                }
                $recordMove->qty = $qty;
            }
            $recordMove->save();

            $mapRes = $this->moveInfo($request->move_id);
            return response()->json([
                'success' => true,
                'code' => 200,
                'data' => [
                    'part_list' => $mapRes,
                    'scan_result' => $this->moveInfo($request->move_id, $code)
                ],
                'message' => ['success']
            ]);
        }
    }
    private function moveInfo($move_id, $part = null)
    {
        $data = InventoryMoveFrom::query()
            ->with('product')
            ->select('z_invmovefrom.product_code', DB::raw('sum(z_invmovefrom.qty) as qty_from'), DB::raw('sum(z_invmoveto.qty) as qty_to'))
            ->leftJoin('adempiere.z_invmoveto', function ($q) {
                $q->on('z_invmoveto.invmove_id', 'z_invmovefrom.invmove_id');
                $q->on('z_invmoveto.product_code', 'z_invmovefrom.product_code');
            })
            ->groupBy('z_invmovefrom.product_code', 'z_invmovefrom.invmove_id')
            ->whereHas('headerMove', function ($q) use ($move_id) {
                $q->where('documentno', $move_id);
            });
        if ($part != null) {
            $data = $data->where('z_invmovefrom.product_code', $part);
            $data = $data->first();

            return [
                'product_code' => $data->product_code,
                'qty_from' => $data->qty_from,
                'qty_to' => $data->qty_to ?? 0,
                'product_name' => $data->product->name,
                'sku' => $data->product->sku,
            ];
        } else {
            $data = $data->get();
            return   $data->map(function ($q) {
                return [
                    'product_code' => $q->product_code,
                    'qty_from' => $q->qty_from,
                    'qty_to' => $q->qty_to ?? 0,
                    'product_name' => $q->product->name,
                    'sku' => $q->product->sku,


                ];
            });
        }
    }
    public function getInOutInfo($move_id)
    {

        $mapData = $this->moveInfo($move_id);
        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => [
                'loaded'
            ],
            'data' => $mapData
        ]);
    }
}
