<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZInvmovefromTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adempiere.z_invmovefrom', function (Blueprint $table) {
            $table->id();
            $table->foreignId('invmove_id')->references('id')->on('adempiere.z_invmove');
            $table->string('product_code');
            $table->string('locator');
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adempiere.z_invmovefrom');
    }
}
