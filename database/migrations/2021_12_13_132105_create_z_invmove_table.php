<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZInvmoveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adempiere.z_invmove', function (Blueprint $table) {
            $table->id();
            $table->string('documentno')->nullable();
            $table->timestamp('document_date');
            $table->string('from_wh')->nullable();
            $table->string('to_wh')->nullable();
            $table->integer('status')->default(1)->comment('1:active,2:complete,3:void');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adempiere.z_invmove');
    }
}
