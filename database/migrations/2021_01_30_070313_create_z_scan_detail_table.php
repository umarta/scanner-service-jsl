<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZScanDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adempiere.z_scan_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('scan_id')->references('id')->on('adempiere.z_scan_header');
            $table->integer('no');
            $table->string('kode_produk');
            $table->string('locator_to_id');
            $table->string('kode_locator_to');
            $table->string('locator_from_id');
            $table->string('kode_locator_from');
            $table->decimal('qty_idem');
            $table->decimal('qty_scan')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adempiere.z_scan_detail');
    }
}
