<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeTrxToScanHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adempiere.z_scan_header', function (Blueprint $table) {
            $table->string('type_trx')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adempiere.z_scan_header', function (Blueprint $table) {
            $table->dropColumn('type_trx');

        });
    }
}
