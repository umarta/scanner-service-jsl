<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZScanHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adempiere.z_scan_header', function (Blueprint $table) {
            $table->id();
            $table->string('transaksi_id')->nullable();
            $table->string('warehouse_from_id')->nullable();
            $table->string('warehouse_to_id');
            $table->string('kode_warehouse_from')->nullable();
            $table->string('kode_warehouse_to');
            $table->string('doc_no');
            $table->string('doc_status');
            $table->string('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adempiere.z_scan_header');
    }
}
