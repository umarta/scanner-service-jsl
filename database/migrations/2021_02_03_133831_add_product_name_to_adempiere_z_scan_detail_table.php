<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductNameToAdempiereZScanDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adempiere.z_scan_detail', function (Blueprint $table) {
            $table->string('product_name')->nullable();
            $table->string('sku')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adempiere.z_scan_detail', function (Blueprint $table) {
            $table->dropColumn('product_name');
            $table->dropColumn('sku');
        });
    }
}
