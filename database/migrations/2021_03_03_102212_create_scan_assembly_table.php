<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScanAssemblyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adempiere.z_scan_assembly', function (Blueprint $table) {
            $table->id();
            $table->foreignId('scan_id')->references('id')->on('adempiere.z_scan_header');
            $table->foreignId('locator_id')->references('m_locator_id')->on('adempiere.m_locator');
            $table->boolean('status_penerimaa')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_assembly');
    }
}
